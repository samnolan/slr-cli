import ./openapi.nix {
  pname = "springer";
  version = "0.1.0.0";
  specFolder = ../spec;
  spec = "springer.yaml";
  baseModule = "Web.Springer";
}
