import ./openapi.nix {
  pname = "dblp";
  version = "0.1.0.0";
  specFolder = ../spec;
  spec = "dblp.yaml";
  baseModule = "Web.DBLP";
}
