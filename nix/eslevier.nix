import ./openapi.nix {
  pname = "eslevier";
  version = "0.1.0.0";
  specFolder = ../spec;
  spec = "eslevier.yaml";
  baseModule = "Web.Eslevier";
}
