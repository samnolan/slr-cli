import ./openapi.nix {
  pname = "semantic-scholar";
  version = "0.1.0.0";
  specFolder = ../spec;
  spec = "semantic.yaml";
  baseModule = "Web.SemanticScholar";
}
