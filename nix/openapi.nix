{ pname
, version
, spec
, specFolder
, baseModule
}:
{ mkDerivation, aeson, base, base64-bytestring, bytestring
, case-insensitive, containers, deepseq, exceptions, hspec
, http-api-data, http-client, http-client-tls, http-media
, http-types, iso8601-time, katip, lib, microlens, mtl, network
, QuickCheck, random, safe-exceptions, semigroups, text, time
, transformers, unordered-containers, vector, pkgs
}:
mkDerivation {
  pname = pname;
  version = version;
  src = specFolder;
  prePatch = ''
    openapi-generator-cli generate -g haskell-http-client -i ${spec} --additional-properties=baseModule=${baseModule},cabalPackage=${pname}
  '';
  isLibrary = true;
  isExecutable = false;
  libraryHaskellDepends = [
    aeson base base64-bytestring bytestring case-insensitive containers
    deepseq exceptions http-api-data http-client http-client-tls
    http-media http-types iso8601-time katip microlens mtl network
    random safe-exceptions text time transformers unordered-containers
    vector
  ];
  libraryToolDepends = [ pkgs.openapi-generator-cli ];
  executableHaskellDepends = [ base ];
  testHaskellDepends = [
    aeson base bytestring containers hspec iso8601-time mtl QuickCheck
    semigroups text time transformers unordered-containers vector
  ];
  homepage = "https://github.com/githubuser/systematic-literature#readme";
  license = lib.licenses.mit;
}
