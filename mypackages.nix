{ pkgs ? import <nixpkgs> {} }:
let
  config = {
    packageOverrides = pkgs: rec {
      haskellPackages = pkgs.haskellPackages.override {
        overrides = haskellPackagesNew: haskellPackagesOld: rec {
          semantic-scholar =
            haskellPackagesNew.callPackage ./nix/semantic-scholar.nix { };
          eslevier =
            haskellPackagesNew.callPackage ./nix/eslevier.nix { };
          dblp =
            haskellPackagesNew.callPackage ./nix/dblp.nix { };
          springer =
            haskellPackagesNew.callPackage ./nix/springer.nix { };
          slr-cli =
            haskellPackagesNew.callPackage ./package.nix { };
        };
      };
    };
  };

  mypkgs = import <nixpkgs> { inherit config; };
in 
  mypkgs
