{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLists #-}

module Lib
    ( someFunc
    ) where

import qualified Web.SemanticScholar as SS

import System.Environment (getArgs)
import qualified Data.Csv as Csv
import Data.Csv ((.:), (.=))
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LBS
import qualified Data.Text as T
import qualified Control.Monad as Monad
import qualified Network.HTTP.Client.TLS as Client
import qualified Data.List as List
import qualified Data.Vector as V
import qualified Data.Maybe as Maybe


data Paper = Paper {
  paperTitle :: T.Text,
  paperDoi :: T.Text,
  include :: Bool
}
  deriving (Eq, Show)

instance Csv.FromNamedRecord Paper where
  parseNamedRecord m = Paper <$> m .: "title" <*> m .: "doi" <*> ( (==T.pack "yes") <$> m .: "include")

instance Csv.ToNamedRecord Paper where
  toNamedRecord (Paper title doi include) = Csv.namedRecord [
        "title" .= title, "doi" .= doi, "include" .= (if include then "yes" :: T.Text else "no") ]

instance Csv.DefaultOrdered Paper where
  headerOrder a = [ "title", "doi", "include" ]
                

someFunc :: IO ()
someFunc = do
  args <- getArgs
  case args of
    [ "snowball", file ] -> do
      contents <- LBS.readFile file
      case Csv.decodeByName contents of
        Right (_, result) -> do
          manager <- Client.newTlsManager
          config <- SS.newConfig 
          papers <- Monad.forM result $ \paper -> do
            result <- SS.dispatchMime' manager config $ SS.paper . SS.Identifier $ paperDoi paper
            case result of
              Left err -> do
                print err
                return []
              Right result -> 
                return $ map (\citation -> Paper (SS.citationTitle citation) (Maybe.fromMaybe ""  (SS.citationDoi citation)) False) (SS.paperResponseCitations result)
          LBS.writeFile "snowball.csv" $ Csv.encodeDefaultOrderedByName (List.nub . concat $ papers)
        Left err -> do
          print err
    _ -> 
      putStrLn "snowball: [file]"
