{ pkgs ? import <nixpkgs> {} }:
let
  nixpkgs = (import ./mypackages.nix {pkgs=pkgs;});
  project = import ./default.nix {};
in
  pkgs.mkShell {
    shellHook = ''
      export NIX_GHC="$(which ghc)"
      export NIX_GHCPKG="$(which ghc-pkg)"
      export NIX_GHC_DOCDIR="$NIX_GHC/../../share/doc/ghc/html"
      export NIX_GHC_LIBDIR="$(ghc --print-libdir)"
    '';
    name = "semantic-scholar-cli-env";
    buildInputs = (project.env.nativeBuildInputs ++ (with nixpkgs; [ 
      (nixpkgs.haskellPackages.ghcWithHoogle (haskellPackages: project.env.nativeBuildInputs))
      bazel
      haskellPackages.hpack 
      cabal2nix 
      haskellPackages.haskell-language-server 
      cabal-install 
      stack 
      haskellPackages.hlint
      haskellPackages.brittany
      openapi-generator-cli
    ]));
  }
