{ mkDerivation, base, bytestring, cassava, hpack, http-client
, http-client-tls, lib, semantic-scholar, text, vector
}:
mkDerivation {
  pname = "slr-cli";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    base bytestring cassava http-client http-client-tls
    semantic-scholar text vector
  ];
  libraryToolDepends = [ hpack ];
  executableHaskellDepends = [
    base bytestring cassava http-client http-client-tls
    semantic-scholar text vector
  ];
  testHaskellDepends = [
    base bytestring cassava http-client http-client-tls
    semantic-scholar text vector
  ];
  prePatch = "hpack";
  homepage = "https://github.com/githubuser/slr-cli#readme";
  license = lib.licenses.bsd3;
}
